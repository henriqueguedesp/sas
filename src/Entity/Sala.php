<?php

namespace UBSValorem\Entity;

class Sala {

    private $idSala;
    private $descricao;
    private $setor;
    
    function getIdSala() {
        return $this->idSala;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getSetor() {
        return $this->setor;
    }

    function setIdSala($idSala) {
        $this->idSala = $idSala;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setSetor($setor) {
        $this->setor = $setor;
    }

        
    function __construct() {
        
    }

}
