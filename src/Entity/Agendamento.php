<?php


namespace UBSValorem\Entity;


class Agendamento {
    private $id;
    private $idUsuario;
    private $idSala;
    private $atividade;
    private $dataInicio;
    private $dataTermino;  
    private $horaInicio;
    private $minutoInicio;
    private $horaTermino;
    private $minutoTermino;
    private $statu;
    private $dataCriacao;
    private $para;
    
    function getId() {
        return $this->id;
    }
    function getPara() {
        return $this->para;
    }

    function setPara($para) {
        $this->para = $para;
    }

    
    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getIdSala() {
        return $this->idSala;
    }

    function getAtividade() {
        return $this->atividade;
    }

    function getDataInicio() {
        return $this->dataInicio;
    }

    function getDataTermino() {
        return $this->dataTermino;
    }

    function getHoraInicio() {
        return $this->horaInicio;
    }

    function getMinutoInicio() {
        return $this->minutoInicio;
    }

    function getHoraTermino() {
        return $this->horaTermino;
    }

    function getMinutoTermino() {
        return $this->minutoTermino;
    }

    function getStatu() {
        return $this->statu;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setIdSala($idSala) {
        $this->idSala = $idSala;
    }

    function setAtividade($atividade) {
        $this->atividade = $atividade;
    }

    function setDataInicio($dataInicio) {
        $this->dataInicio = $dataInicio;
    }

    function setDataTermino($dataTermino) {
        $this->dataTermino = $dataTermino;
    }

    function setHoraInicio($horaInicio) {
        $this->horaInicio = $horaInicio;
    }

    function setMinutoInicio($minutoInicio) {
        $this->minutoInicio = $minutoInicio;
    }

    function setHoraTermino($horaTermino) {
        $this->horaTermino = $horaTermino;
    }

    function setMinutoTermino($minutoTermino) {
        $this->minutoTermino = $minutoTermino;
    }

    function setStatu($statu) {
        $this->statu = $statu;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

        
    function __construct() {
        
    }

}
