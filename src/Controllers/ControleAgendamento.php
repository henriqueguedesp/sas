<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloSala;
use UBSValorem\Models\ModeloAgendamento;
use UBSValorem\Entity\Sala;
use UBSValorem\Entity\Agendamento;

class ControleAgendamento {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function cancelarAgendamento($id) {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloAgendamento();
            $modelo->cancelar($id);
            echo "<script> alert('Agendamento cancelado com sucesso!'); "
            . " location.href='/sas/public_html/agendamento';</script>";

            //return $this->response->setContent($this->twig->render('AgendamentoData.html.twig', array('user' => $usuario, 'agenda' => $agenda, 'sala' => $sala, 'data' => $data)));
        } else {
            $this->redireciona('/login');
        }
    }

    public function realizarAgendamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $agendamento = new Agendamento();
            $modelo = new ModeloAgendamento();
            $data = $this->request->get('dataE');
            $data = implode("/", array_reverse(explode("/", $data)));
            $agendamento->setDataInicio($data);
            
              $agendamento->setHoraInicio($this->request->get('horaInicio'));
              $agendamento->setHoraTermino($this->request->get('horaTermino'));
              $agendamento->setIdUsuario($usuario->idUsuario);
              $agendamento->setIdSala($this->request->get('idSala'));
              $agendamento->setAtividade('null');
              $agendamento->setPara($this->request->get('para'));
              if (strcmp($agendamento->getHoraInicio(), $agendamento->getHoraTermino()) > 0) {
              //HORA INICO MAIOR QUE HORA TERMINO
              echo 10;
              } else {
              //ok
              //fazer verificação se tem horario
              $verificacao = $modelo->verificaAgendamento($agendamento);
              if ($verificacao) {
              //não tem horario disponivel para esse agendamento desejado
              echo 20;
              } else {
              $modelo->agendar($agendamento);
              echo 0;
              }
              }
            //return $this->response->setContent($this->twig->render('AgendamentoData.html.twig', array('user' => $usuario, 'agenda' => $agenda, 'sala' => $sala, 'data' => $data)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

    public function realizandoAgendamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $sala = new Sala();
            $data = $this->request->get('dataC');

            $sala->setIdSala($this->request->get('sala'));
            $modelo = new ModeloSala();
            $info = $modelo->sala($sala->getIdSala());
            $sala->setDescricao($info[0]->descricao);
            $sala->setSetor($info[0]->setor);

            $modelo = new ModeloAgendamento();
            $agenda = $modelo->verificaData($data, $sala->getIdSala());

            $data = date('d/m/Y', strtotime($data));

            return $this->response->setContent($this->twig->render('AgendamentoData.html.twig', array('user' => $usuario, 'agenda' => $agenda, 'sala' => $sala, 'data' => $data)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

    public function agendamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloSala();
            $salas = $modelo->salas();
            $modelo = new ModeloAgendamento();
            $agendamentos = $modelo->agendamentosUsuario($usuario->idUsuario);
            //return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('dados' => $dados, 'user' => $usuario)));
            return $this->response->setContent($this->twig->render('Agendamento.html.twig', array('user' => $usuario, 'salas' => $salas, 'agendamentos' => $agendamentos)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

}
