<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloSala;

class ControleSala {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function desativarSala($id) {
        $modelo = new ModeloSala();
        $modelo->desativar($id);
        echo "<script> alert('Sala desativada com sucesso!'); "
        . " location.href='/sas/public_html/controleSala';</script>";
    }

    public function ativarSala($id) {
        $modelo = new ModeloSala();
        $modelo->ativar($id);
        echo "<script> alert('Sala ativada com sucesso!'); "
        . " location.href='/sas/public_html/controleSala';</script>";
    }

    public function editarSala($id) {
        $sala = $this->request->get('sala');
        $setor = $this->request->get('setor');
        $modelo = new ModeloSala();
        $verica = $modelo->verificaSalaEditar($sala, $setor, $id);
        if ($verica) {
            echo 1;
        } else {
            $modelo->atualizar($sala, $setor, $id);
            echo 0;
        }
    }

    public function cadastrarSala() {
        $sala = $this->request->get('sala');
        $setor = $this->request->get('setor');
        $modelo = new ModeloSala();
        $verica = $modelo->verificaSala($sala, $setor);
        if ($verica) {
            echo 1;
        } else {
            $modelo->cadastrar($sala, $setor);
            echo 0;
        }
    }

}
