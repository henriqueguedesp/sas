<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloSala;
use UBSValorem\Models\ModeloAgendamento;

class ControleIndex {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function erro404() {

        return $this->response->setContent($this->twig->render('Erro404.html.twig'));
    }

    public function controleSala() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $modelo = new ModeloSala();
            $salas = $modelo->salas();
            //return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('dados' => $dados, 'user' => $usuario)));
            return $this->response->setContent($this->twig->render('ControleSala.html.twig', array('user' => $usuario, 'salas' => $salas)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

    public function dashboard() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloAgendamento();

            $agenda = $modelo->agendamentos();
            $controle = $modelo->maiorDataAgendamento();
            //aqui pego a maio data de agendamento e converto somente para o Ano
            $ultimoAno = date('Y', strtotime($controle->dataInicio));
            ///comeca controle de exibição
            date_default_timezone_set('America/Sao_Paulo');

            $anoAtual = date('Y');
            $mesAtual = date('m');

            $anoMes = array();
            //$final = array();
            $mes = array();
            $mesU = array();
            for ($i = $anoAtual; $i <= $ultimoAno; $i++) {
                $anoMes[] = $i;
                // print_r($ano);

                if ($i == $anoAtual) {
                    for ($j = 1; $j <= 12; $j++) {
                        if ($j == $mesAtual) {
                            //$mes[] = $j;
                            $data01 = date('Y-m-d');
                            $data02 = "" . $i . "/" . $j . "/31";
                            // $anoMes = array_merge($ano, $mes);

                            $dados = $modelo->agendamentoMesAno($data01, $data02);
                            $mes[] = $dados;
                            // array_push($mes, $dados);
                        } else {
                            $dados = null;
                            $mes[] = $dados;
                        }
                    }
                    array_push($anoMes, $mes);
                } else {
                    for ($j = 1; $j <= 12; $j++) {
                        //$mes[] = $j;
                        $data01 = "" . $i . "-" . $j . "-01";
                        $data02 = "" . $i . "-" . $j . "-31";
                        $dados = $modelo->agendamentoMesAno($data01, $data02);
                        $mesU[] = $dados;
                    }
                    array_push($anoMes, $mesU);
                }



                //$final = array_merge($anoMes, $mes);
            }
            //array_push($ano, $mes);
            //echo "Push: ", var_export($mes, true), "\n";
            //print_r($final);
    //       print_r($anoMes);
            // print_r('<br>');
            //print_r('<br>');
            return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('user' => $usuario, 'agenda' => $agenda, 'anoAtual' => $anoAtual, 'anoUltimo' => $ultimoAno,'anoMes' => $anoMes)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
