<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\Agendamento;

class ModeloAgendamento {

    public function cancelar($id) {
        try {
            $sql = "update  agendamento set status = 0 where idAgendamento = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function agendamentosUsuario($idUsuario) {
        try {
            $sql = "select * from agendamento as a, sala as s where a.idUsuario = :idUsuario and a.status = 1 and a.dataInicio>= curdate()  "
                    . "and s.idSala = a.idSala order by a.dataInicio asc";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idUsuario', $idUsuario);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaAgendamento(Agendamento $agendamento) {
        try {
            $sql = "select * from agendamento where "
                    . "dataInicio = :data and"
                    . " idSala =  :idSala and "
                    . " horaTermino > :horaInicio and "
                    . "horaInicio < :horaTermino and status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':data', $agendamento->getDataInicio());
            $p_sql->bindValue(':idSala', $agendamento->getIdSala());
            $p_sql->bindValue(':horaInicio', $agendamento->getHoraInicio());
            $p_sql->bindValue(':horaTermino', $agendamento->getHoraTermino());
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function agendar(Agendamento $agendamento) {
        try {
            $sql = "insert into agendamento (idUsuario, idSala, atividade, dataInicio, horaInicio, horaTermino, status, dataCriacao,para)"
                    . " values"
                    . " (:idUsuario,:idSala,:atividade, :dataInicio, :horaInicio, :horaTermino, 1, now(), :para)";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':idUsuario', $agendamento->getIdUsuario());
            $p_sql->bindValue(':idSala', $agendamento->getIdSala());
            $p_sql->bindValue(':atividade', $agendamento->getDataInicio());
            $p_sql->bindValue(':dataInicio', $agendamento->getDataInicio());
            $p_sql->bindValue(':horaInicio', $agendamento->getHoraInicio());
            $p_sql->bindValue(':horaTermino', $agendamento->getHoraTermino());
            $p_sql->bindValue(':para', $agendamento->getPara());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
    
    public function agendamentoMesAno($data01,$data02) {
        try {
            //$sql = "select a.dataInicio from agendamento as a, usuario as u, sala as s where :data <= a.dataInicio and a.status = 1 and u.idUsuario = a.idUsuario "
           //         . "and s.idSala = a.idSala group by a.dataInicio order by a.dataInicio desc ";
            $sql = "select * from agendamento as a, usuario as u, sala as s 
where a.status = 1 and u.idUsuario = a.idUsuario and s.idSala = a.idSala and (a.dataInicio between :data01 and :data02 )order by a.dataInicio asc ;";
           // $sql = "select a.dataInicio from agendamento as a, usuario as u, sala as s where a.status = 1 and u.idUsuario = a.idUsuario "
             //       . "and s.idSala = a.idSala (dataInicio between :data01 and :data02 )order by a.dataInicio desc ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            //$p_sql->bindValue(':data01', $data01);
            //$p_sql->bindValue(':data02', $data02);
            $p_sql->bindValue(':data01', $data01);
            $p_sql->bindValue(':data02', $data02);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
    public function maiorDataAgendamento() {
        try {
            date_default_timezone_set('America/Sao_Paulo');
            $date = date('Y-m-d');
            $sql = "select a.dataInicio from agendamento as a, usuario as u, sala as s where :data <= a.dataInicio and a.status = 1 and u.idUsuario = a.idUsuario "
                    . "and s.idSala = a.idSala group by a.dataInicio order by a.dataInicio desc ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':data', $date);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    

    public function agendamentos() {
        try {
            date_default_timezone_set('America/Sao_Paulo');
            $date = date('Y-m-d');
            $sql = "select * from agendamento as a, usuario as u, sala as s where :data <= a.dataInicio and a.status = 1 and u.idUsuario = a.idUsuario "
                    . "and s.idSala = a.idSala order by a.dataInicio asc ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':data', $date);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaData($data, $idSala) {
        try {
            $sql = "select * from agendamento as a, usuario as u where (a.dataInicio = :data or a.dataTermino = :data) and a.idSala =  :idSala and "
                    . "u.idUsuario = a.idUsuario and a.status = 1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':data', $data);
            $p_sql->bindValue(':idSala', $idSala);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
