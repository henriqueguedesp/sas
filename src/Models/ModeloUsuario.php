<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\Usuario;

class ModeloUsuario {

     public function ativar($id) {
        try {
            $sql = "update  usuario set status = 1"
                    . " where idUsuario =  :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
     public function desativar($id) {
        try {
            $sql = "update usuario set status = 0"
                    . " where idUsuario =  :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
    
     public function atualizar(Usuario $usuario) {
        try {
            $sql = "update  usuario set nome = :nome, usuario = :usuario, senha = :senha, funcao = :funcao, email = :email, tipo = :tipo "
                    . " where idUsuario =  :idUsuario ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $usuario->getNome());
            $p_sql->bindValue(':usuario', $usuario->getUsuario());
            $p_sql->bindValue(':senha', $usuario->getSenha());
            $p_sql->bindValue(':funcao', $usuario->getFuncao());
            $p_sql->bindValue(':email', $usuario->getEmail());
            $p_sql->bindValue(':idUsuario', $usuario->getIdUsuario());
            $p_sql->bindValue(':tipo', $usuario->getTipo());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
    
    public function usuarios($id) {
        try {
            $sql = "select * from usuario where idUsuario != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    
    public function cadastrar(Usuario $usuario) {
        try {
            $sql = "insert into usuario (nome,usuario,senha,funcao,email, status, tipo) "
                    . "values "
                    . "(:nome,:usuario,:senha,:funcao,:email,1,:tipo)";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $usuario->getNome());
            $p_sql->bindValue(':usuario', $usuario->getUsuario());
            $p_sql->bindValue(':senha', $usuario->getSenha());
            $p_sql->bindValue(':funcao', $usuario->getFuncao());
            $p_sql->bindValue(':email', $usuario->getEmail());
            $p_sql->bindValue(':tipo', $usuario->getTipo());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function validaLogin($usuario, $senha) {
        try {
            $sql = "select tipo,idUsuario,usuario,email,nome,funcao from usuario where usuario = :usuario and senha = :senha and status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->bindValue(":senha", $senha);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    function __construct() {
        
    }

}
